# Für was braucht man eigentlich systemd nach dem booten?

## Inhalt
 
- Was ist systemd?
- Struktur der systemd manpages
- Was sind unit files
- Eigenen Service anlegen
- manuell starten und stoppen
- Informationenen über den laufenden Prozess
- automatisch Starten
- Einschränkungensmöglichkeiten

[Folien im pdf Format](systemd nach dem booten.pdf)

Auf dem Branch `revealjs` ist die Präsentation im HTML-Format mit revealjs Umgebung enthalten. 
